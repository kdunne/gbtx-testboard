EESchema Schematic File Version 4
LIBS:gbtx-testboard-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L gbtx:GBTX U?
U 6 1 5D75D2A0
P 1700 2250
F 0 "U?" H 1725 3465 50  0000 C CNN
F 1 "GBTX" H 1725 3374 50  0000 C CNN
F 2 "" H 1700 2650 50  0001 C CNN
F 3 "" H 1700 2650 50  0001 C CNN
	6    1700 2250
	1    0    0    -1  
$EndComp
$Comp
L gbtx:GBTX U?
U 7 1 5D761800
P 1700 5400
F 0 "U?" H 1675 6615 50  0000 C CNN
F 1 "GBTX" H 1675 6524 50  0000 C CNN
F 2 "" H 1700 5800 50  0001 C CNN
F 3 "" H 1700 5800 50  0001 C CNN
	7    1700 5400
	1    0    0    -1  
$EndComp
$Comp
L gbtx:GBTX U?
U 8 1 5D783BCA
P 8400 1800
F 0 "U?" H 8750 3015 50  0000 C CNN
F 1 "GBTX" H 8750 2924 50  0000 C CNN
F 2 "" H 8400 2200 50  0001 C CNN
F 3 "" H 8400 2200 50  0001 C CNN
	8    8400 1800
	1    0    0    -1  
$EndComp
Text HLabel 900  1100 0    50   Input ~ 0
VCC1V5
Wire Wire Line
	900  1100 900  1450
Wire Wire Line
	900  3750 1150 3750
Text HLabel 2600 1100 2    50   Input ~ 0
VCC1V5
Wire Wire Line
	2600 1100 2600 1450
Wire Wire Line
	2600 2350 2300 2350
Wire Wire Line
	2300 1450 2600 1450
Connection ~ 2600 1450
Wire Wire Line
	2600 1450 2600 1550
Wire Wire Line
	2300 1550 2600 1550
Connection ~ 2600 1550
Wire Wire Line
	2600 1550 2600 1650
Wire Wire Line
	2300 1650 2600 1650
Connection ~ 2600 1650
Wire Wire Line
	2300 1750 2600 1750
Wire Wire Line
	2600 1650 2600 1750
Connection ~ 2600 1750
Wire Wire Line
	2600 1750 2600 1850
Wire Wire Line
	2300 1850 2600 1850
Connection ~ 2600 1850
Wire Wire Line
	2600 1850 2600 1950
Wire Wire Line
	2300 1950 2600 1950
Connection ~ 2600 1950
Wire Wire Line
	2600 1950 2600 2050
Wire Wire Line
	2300 2050 2600 2050
Connection ~ 2600 2050
Wire Wire Line
	2600 2050 2600 2150
Wire Wire Line
	2300 2150 2600 2150
Connection ~ 2600 2150
Wire Wire Line
	2600 2150 2600 2250
Wire Wire Line
	2300 2250 2600 2250
Connection ~ 2600 2250
Wire Wire Line
	2600 2250 2600 2350
Wire Wire Line
	1150 1450 900  1450
Connection ~ 900  1450
Wire Wire Line
	900  1450 900  1550
Wire Wire Line
	1150 1550 900  1550
Connection ~ 900  1550
Wire Wire Line
	900  1550 900  1650
Wire Wire Line
	1150 1650 900  1650
Connection ~ 900  1650
Wire Wire Line
	900  1650 900  1750
Wire Wire Line
	1150 1750 900  1750
Connection ~ 900  1750
Wire Wire Line
	900  1750 900  1850
Wire Wire Line
	1150 1850 900  1850
Connection ~ 900  1850
Wire Wire Line
	900  1850 900  1950
Wire Wire Line
	1150 1950 900  1950
Connection ~ 900  1950
Wire Wire Line
	900  1950 900  2050
Wire Wire Line
	1150 2050 900  2050
Connection ~ 900  2050
Wire Wire Line
	900  2050 900  2150
Wire Wire Line
	1150 2150 900  2150
Connection ~ 900  2150
Wire Wire Line
	900  2150 900  2250
Wire Wire Line
	1150 2250 900  2250
Connection ~ 900  2250
Wire Wire Line
	900  2250 900  2350
Wire Wire Line
	1150 2350 900  2350
Connection ~ 900  2350
Wire Wire Line
	900  2350 900  2450
Wire Wire Line
	1150 2450 900  2450
Connection ~ 900  2450
Wire Wire Line
	900  2450 900  2550
Wire Wire Line
	1150 2550 900  2550
Connection ~ 900  2550
Wire Wire Line
	900  2550 900  2650
Wire Wire Line
	1150 2650 900  2650
Connection ~ 900  2650
Wire Wire Line
	900  2650 900  2750
Wire Wire Line
	1150 2750 900  2750
Connection ~ 900  2750
Wire Wire Line
	900  2750 900  2850
Wire Wire Line
	1150 2850 900  2850
Connection ~ 900  2850
Wire Wire Line
	900  2850 900  2950
Wire Wire Line
	1150 2950 900  2950
Connection ~ 900  2950
Wire Wire Line
	900  2950 900  3050
Wire Wire Line
	1150 3050 900  3050
Connection ~ 900  3050
Wire Wire Line
	900  3050 900  3250
Wire Wire Line
	1150 3250 900  3250
Connection ~ 900  3250
Wire Wire Line
	900  3250 900  3350
Wire Wire Line
	1150 3350 900  3350
Connection ~ 900  3350
Wire Wire Line
	900  3350 900  3450
Wire Wire Line
	1150 3450 900  3450
Connection ~ 900  3450
Wire Wire Line
	900  3450 900  3550
Wire Wire Line
	1150 3550 900  3550
Connection ~ 900  3550
Wire Wire Line
	900  3550 900  3650
Wire Wire Line
	1150 3650 900  3650
Connection ~ 900  3650
Wire Wire Line
	900  3650 900  3750
NoConn ~ 1150 3950
Text HLabel 800  7550 0    50   Input ~ 0
GND
Wire Wire Line
	800  7550 800  6700
Wire Wire Line
	800  4700 1100 4700
Wire Wire Line
	1100 6700 800  6700
Connection ~ 800  6700
Wire Wire Line
	800  6700 800  6600
Wire Wire Line
	1100 6600 800  6600
Connection ~ 800  6600
Wire Wire Line
	800  6600 800  6500
Wire Wire Line
	1100 6500 800  6500
Connection ~ 800  6500
Wire Wire Line
	800  6500 800  6400
Wire Wire Line
	1100 6400 800  6400
Connection ~ 800  6400
Wire Wire Line
	800  6400 800  6300
Wire Wire Line
	1100 6300 800  6300
Connection ~ 800  6300
Wire Wire Line
	800  6300 800  6100
Wire Wire Line
	1100 6100 800  6100
Connection ~ 800  6100
Wire Wire Line
	800  6100 800  6000
Wire Wire Line
	1100 6000 800  6000
Connection ~ 800  6000
Wire Wire Line
	800  6000 800  5900
Wire Wire Line
	1100 5900 800  5900
Connection ~ 800  5900
Wire Wire Line
	800  5900 800  5800
Wire Wire Line
	1100 5800 800  5800
Connection ~ 800  5800
Wire Wire Line
	800  5800 800  5600
Wire Wire Line
	1100 5600 800  5600
Connection ~ 800  5600
Wire Wire Line
	800  5600 800  5500
Wire Wire Line
	1100 5500 800  5500
Connection ~ 800  5500
Wire Wire Line
	800  5500 800  5400
Wire Wire Line
	1100 5400 800  5400
Connection ~ 800  5400
Wire Wire Line
	800  5400 800  5300
Wire Wire Line
	1100 5300 800  5300
Connection ~ 800  5300
Wire Wire Line
	800  5300 800  5200
Wire Wire Line
	1100 5200 800  5200
Connection ~ 800  5200
Wire Wire Line
	800  5200 800  5000
Wire Wire Line
	1100 5000 800  5000
Connection ~ 800  5000
Wire Wire Line
	800  5000 800  4900
Wire Wire Line
	1100 4900 800  4900
Connection ~ 800  4900
Wire Wire Line
	800  4900 800  4800
Wire Wire Line
	1100 4800 800  4800
Connection ~ 800  4800
Wire Wire Line
	800  4800 800  4700
Text HLabel 2550 7550 2    50   Input ~ 0
GND
Wire Wire Line
	2550 7550 2550 7300
Wire Wire Line
	2550 4500 2250 4500
Wire Wire Line
	2250 4600 2550 4600
Connection ~ 2550 4600
Wire Wire Line
	2550 4600 2550 4500
Wire Wire Line
	2250 4700 2550 4700
Connection ~ 2550 4700
Wire Wire Line
	2550 4700 2550 4600
Wire Wire Line
	2250 4800 2550 4800
Connection ~ 2550 4800
Wire Wire Line
	2550 4800 2550 4700
Wire Wire Line
	2250 4900 2550 4900
Connection ~ 2550 4900
Wire Wire Line
	2550 4900 2550 4800
Wire Wire Line
	2250 5000 2550 5000
Connection ~ 2550 5000
Wire Wire Line
	2550 5000 2550 4900
Wire Wire Line
	2250 5100 2550 5100
Connection ~ 2550 5100
Wire Wire Line
	2550 5100 2550 5000
Wire Wire Line
	2250 5200 2550 5200
Connection ~ 2550 5200
Wire Wire Line
	2550 5200 2550 5100
Wire Wire Line
	2250 5300 2550 5300
Connection ~ 2550 5300
Wire Wire Line
	2550 5300 2550 5200
Wire Wire Line
	2250 5400 2550 5400
Connection ~ 2550 5400
Wire Wire Line
	2550 5400 2550 5300
Wire Wire Line
	2250 5500 2550 5500
Connection ~ 2550 5500
Wire Wire Line
	2550 5500 2550 5400
Wire Wire Line
	2250 5600 2550 5600
Connection ~ 2550 5600
Wire Wire Line
	2550 5600 2550 5500
Wire Wire Line
	2250 5700 2550 5700
Connection ~ 2550 5700
Wire Wire Line
	2550 5700 2550 5600
Wire Wire Line
	2250 5800 2550 5800
Connection ~ 2550 5800
Wire Wire Line
	2550 5800 2550 5700
Wire Wire Line
	2250 5900 2550 5900
Connection ~ 2550 5900
Wire Wire Line
	2550 5900 2550 5800
Wire Wire Line
	2250 6000 2550 6000
Connection ~ 2550 6000
Wire Wire Line
	2550 6000 2550 5900
Wire Wire Line
	2250 6100 2550 6100
Connection ~ 2550 6100
Wire Wire Line
	2550 6100 2550 6000
Wire Wire Line
	2250 6200 2550 6200
Connection ~ 2550 6200
Wire Wire Line
	2550 6200 2550 6100
Wire Wire Line
	2250 6300 2550 6300
Connection ~ 2550 6300
Wire Wire Line
	2550 6300 2550 6200
Wire Wire Line
	2250 6400 2550 6400
Connection ~ 2550 6400
Wire Wire Line
	2550 6400 2550 6300
Wire Wire Line
	2250 6500 2550 6500
Connection ~ 2550 6500
Wire Wire Line
	2550 6500 2550 6400
Wire Wire Line
	2250 6600 2550 6600
Connection ~ 2550 6600
Wire Wire Line
	2550 6600 2550 6500
Wire Wire Line
	2250 6700 2550 6700
Connection ~ 2550 6700
Wire Wire Line
	2550 6700 2550 6600
Wire Wire Line
	2250 6800 2550 6800
Connection ~ 2550 6800
Wire Wire Line
	2550 6800 2550 6700
Wire Wire Line
	2250 6900 2550 6900
Connection ~ 2550 6900
Wire Wire Line
	2550 6900 2550 6800
Wire Wire Line
	2250 7000 2550 7000
Connection ~ 2550 7000
Wire Wire Line
	2550 7000 2550 6900
Wire Wire Line
	2250 7100 2550 7100
Connection ~ 2550 7100
Wire Wire Line
	2550 7100 2550 7000
Wire Wire Line
	2250 7200 2550 7200
Connection ~ 2550 7200
Wire Wire Line
	2550 7200 2550 7100
Wire Wire Line
	2250 7300 2550 7300
Connection ~ 2550 7300
Wire Wire Line
	2550 7300 2550 7200
NoConn ~ 7850 2600
NoConn ~ 7850 2500
NoConn ~ 7850 2400
NoConn ~ 7850 2300
NoConn ~ 6100 5600
NoConn ~ 6100 5700
NoConn ~ 6100 5900
NoConn ~ 6100 6000
Text HLabel 9950 3100 2    50   Input ~ 0
VCC1V5
$Comp
L Device:R_Small R?
U 1 1 5DC3D910
P 9450 3100
F 0 "R?" V 9254 3100 50  0000 C CNN
F 1 "4k7" V 9345 3100 50  0000 C CNN
F 2 "" H 9450 3100 50  0001 C CNN
F 3 "~" H 9450 3100 50  0001 C CNN
	1    9450 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5DC3ED0F
P 9450 3400
F 0 "R?" V 9254 3400 50  0000 C CNN
F 1 "4k7" V 9345 3400 50  0000 C CNN
F 2 "" H 9450 3400 50  0001 C CNN
F 3 "~" H 9450 3400 50  0001 C CNN
	1    9450 3400
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5DC3F4F6
P 9450 3700
F 0 "R?" V 9254 3700 50  0000 C CNN
F 1 "4k7" V 9345 3700 50  0000 C CNN
F 2 "" H 9450 3700 50  0001 C CNN
F 3 "~" H 9450 3700 50  0001 C CNN
	1    9450 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5DC3FD1E
P 9450 4000
F 0 "R?" V 9254 4000 50  0000 C CNN
F 1 "750" V 9345 4000 50  0000 C CNN
F 2 "" H 9450 4000 50  0001 C CNN
F 3 "~" H 9450 4000 50  0001 C CNN
	1    9450 4000
	0    1    1    0   
$EndComp
Text Label 8750 3100 0    50   ~ 0
LDSDA
Text Label 8750 3400 0    50   ~ 0
LDSCL
Text Label 8750 3700 0    50   ~ 0
LDRESET
Text Label 8750 4000 0    50   ~ 0
VCC1V5_PU_U3
Wire Wire Line
	8750 3100 9350 3100
Wire Wire Line
	8750 3400 9350 3400
Wire Wire Line
	8750 3700 9350 3700
Wire Wire Line
	8750 4000 9350 4000
Wire Wire Line
	9550 4000 9950 4000
Wire Wire Line
	9950 4000 9950 3700
Wire Wire Line
	9550 3100 9950 3100
Wire Wire Line
	9550 3400 9950 3400
Connection ~ 9950 3400
Wire Wire Line
	9950 3400 9950 3100
Wire Wire Line
	9550 3700 9950 3700
Connection ~ 9950 3700
Wire Wire Line
	9950 3700 9950 3400
Text HLabel 9950 4350 2    50   Input ~ 0
GND
$Comp
L Device:R_Small R?
U 1 1 5DC88054
P 9450 4350
F 0 "R?" V 9254 4350 50  0000 C CNN
F 1 "750" V 9345 4350 50  0000 C CNN
F 2 "" H 9450 4350 50  0001 C CNN
F 3 "~" H 9450 4350 50  0001 C CNN
	1    9450 4350
	0    1    1    0   
$EndComp
Text Label 8750 4350 0    50   ~ 0
GND_PD_U3
Wire Wire Line
	9550 4350 9950 4350
Wire Wire Line
	8750 4350 9350 4350
Text Label 7450 1500 0    50   ~ 0
LDSDA
Text Label 7450 1600 0    50   ~ 0
LDSCL
Text Label 7450 1700 0    50   ~ 0
LDRESET
Text Label 10450 2200 2    50   ~ 0
VCC1V5_PU_U3
Text Label 7200 2100 0    50   ~ 0
VCC1V5_PU_U3
Wire Wire Line
	7200 2100 7850 2100
Wire Wire Line
	7450 1700 7850 1700
Wire Wire Line
	7450 1600 7850 1600
Wire Wire Line
	7450 1500 7850 1500
Text Label 10450 2500 2    50   ~ 0
GND_PD_U3
Wire Wire Line
	9650 2200 10450 2200
Wire Wire Line
	10450 2500 9900 2500
Wire Wire Line
	9650 2300 9900 2300
Wire Wire Line
	9900 2300 9900 2400
Connection ~ 9900 2500
Wire Wire Line
	9900 2500 9650 2500
Wire Wire Line
	9650 2400 9900 2400
Connection ~ 9900 2400
Wire Wire Line
	9900 2400 9900 2500
Text Label 10450 1800 2    50   ~ 0
VCC1V5_PU_U3
Wire Wire Line
	10450 1800 9650 1800
Text Label 10450 1600 2    50   ~ 0
GND_PD_U3
Wire Wire Line
	10450 1600 9900 1600
Wire Wire Line
	9650 1100 9900 1100
Wire Wire Line
	9900 1100 9900 1300
Connection ~ 9900 1600
Wire Wire Line
	9900 1600 9650 1600
Wire Wire Line
	9900 1500 9650 1500
Connection ~ 9900 1500
Wire Wire Line
	9900 1500 9900 1600
Wire Wire Line
	9650 1400 9900 1400
Connection ~ 9900 1400
Wire Wire Line
	9900 1400 9900 1500
Wire Wire Line
	9900 1300 9650 1300
Connection ~ 9900 1300
Wire Wire Line
	9900 1300 9900 1400
Text Label 10450 1000 2    50   ~ 0
VCC1V5_PU_U3
Wire Wire Line
	10450 1000 9650 1000
$Comp
L gbtx:Header3 U?
U 1 1 5DDEA68B
P 10100 4800
F 0 "U?" H 10278 4751 50  0000 L CNN
F 1 "Header3" H 10278 4660 50  0000 L CNN
F 2 "" H 10100 4800 50  0001 C CNN
F 3 "" H 10100 4800 50  0001 C CNN
	1    10100 4800
	1    0    0    -1  
$EndComp
$Comp
L gbtx:Header3 U?
U 1 1 5DDED7E0
P 10100 5400
F 0 "U?" H 10278 5351 50  0000 L CNN
F 1 "Header3" H 10278 5260 50  0000 L CNN
F 2 "" H 10100 5400 50  0001 C CNN
F 3 "" H 10100 5400 50  0001 C CNN
	1    10100 5400
	1    0    0    -1  
$EndComp
$Comp
L gbtx:Header4 U?
U 1 1 5DDF1117
P 10000 5950
F 0 "U?" H 10278 5851 50  0000 L CNN
F 1 "Header4" H 10278 5760 50  0000 L CNN
F 2 "" H 10000 5950 50  0001 C CNN
F 3 "" H 10000 5950 50  0001 C CNN
	1    10000 5950
	1    0    0    -1  
$EndComp
Text Label 9200 4800 0    50   ~ 0
TESTOUTPUT
Text Label 9200 4900 0    50   ~ 0
TESTCLOCKOUT
Text HLabel 9200 5000 0    50   Input ~ 0
GND
Text HLabel 9200 6150 0    50   Input ~ 0
GND
Text HLabel 9200 5400 0    50   Input ~ 0
VCC1V5
Text HLabel 9200 5600 0    50   Input ~ 0
VCC3V3
Text Label 9200 5500 0    50   ~ 0
EFUSE_POWER
Text Label 9200 5950 0    50   ~ 0
SDA
Text Label 9200 6050 0    50   ~ 0
SCL
Text Label 9200 6250 0    50   ~ 0
EFUSE_PULSE
Wire Wire Line
	9200 6250 9950 6250
Wire Wire Line
	9200 6150 9950 6150
Wire Wire Line
	9200 6050 9950 6050
Wire Wire Line
	9200 5950 9950 5950
Wire Wire Line
	9200 5600 9950 5600
Wire Wire Line
	9200 5500 9950 5500
Wire Wire Line
	9200 5400 9950 5400
Wire Wire Line
	9200 5000 9950 5000
Wire Wire Line
	9200 4900 9950 4900
Wire Wire Line
	9200 4800 9950 4800
$Comp
L gbtx:GBTX U?
U 9 1 5D78F2CE
P 5350 6400
F 0 "U?" H 5450 7615 50  0000 C CNN
F 1 "GBTX" H 5450 7524 50  0000 C CNN
F 2 "" H 5350 6800 50  0001 C CNN
F 3 "" H 5350 6800 50  0001 C CNN
	9    5350 6400
	1    0    0    -1  
$EndComp
Text HLabel 4550 5600 0    50   Output ~ 0
GBTX_RXRDY
Text HLabel 4550 5700 0    50   Output ~ 0
GBTX_RXDATAVALID
Text HLabel 4550 5900 0    50   Input ~ 0
SFP+_1_RD_P
Text HLabel 4550 6000 0    50   Input ~ 0
SFP+_1_RD_N
Text HLabel 4550 6200 0    50   Input ~ 0
VCC1V5_PU_U3
Text HLabel 4550 6300 0    50   Input ~ 0
GND_PD_U3
Wire Wire Line
	4800 6300 4550 6300
Wire Wire Line
	4550 6200 4800 6200
Wire Wire Line
	4550 6000 4800 6000
Wire Wire Line
	4550 5900 4800 5900
Wire Wire Line
	4550 5700 4800 5700
Wire Wire Line
	4550 5600 4800 5600
Text Label 7450 1000 0    50   ~ 0
SDA
Text Label 7450 1100 0    50   ~ 0
SCL
Wire Wire Line
	7450 1000 7850 1000
Wire Wire Line
	7450 1100 7850 1100
Text Notes 750  750  0    129  ~ 0
GBTx Power and Config
$EndSCHEMATC
