EESchema Schematic File Version 4
LIBS:gbtx-testboard-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L gbtx:igloo U?
U 1 1 5DDA7835
P 1700 1450
F 0 "U?" H 1400 1400 50  0000 L CNN
F 1 "igloo" H 1950 1400 50  0000 L CNN
F 2 "" H 1700 1450 50  0001 C CNN
F 3 "" H 1700 1450 50  0001 C CNN
	1    1700 1450
	1    0    0    -1  
$EndComp
$Comp
L gbtx:igloo U?
U 2 1 5DDAA549
P 4850 1600
F 0 "U?" H 4550 1550 50  0000 L CNN
F 1 "igloo" H 5200 1550 50  0000 L CNN
F 2 "" H 4850 1600 50  0001 C CNN
F 3 "" H 4850 1600 50  0001 C CNN
	2    4850 1600
	1    0    0    -1  
$EndComp
$Comp
L gbtx:igloo U?
U 3 1 5DDABAD1
P 7600 1850
F 0 "U?" H 7300 1800 50  0000 L CNN
F 1 "igloo" H 7800 1800 50  0000 L CNN
F 2 "" H 7600 1850 50  0001 C CNN
F 3 "" H 7600 1850 50  0001 C CNN
	3    7600 1850
	1    0    0    -1  
$EndComp
$Comp
L gbtx:igloo U?
U 4 1 5DDADA11
P 10100 1550
F 0 "U?" H 9750 1500 50  0000 L CNN
F 1 "igloo" H 10300 1500 50  0000 L CNN
F 2 "" H 10100 1550 50  0001 C CNN
F 3 "" H 10100 1550 50  0001 C CNN
	4    10100 1550
	1    0    0    -1  
$EndComp
$Comp
L gbtx:igloo U?
U 5 1 5DDAF313
P 1650 5450
F 0 "U?" H 1300 5400 50  0000 C CNN
F 1 "igloo" H 2000 5400 50  0000 C CNN
F 2 "" H 1650 5450 50  0001 C CNN
F 3 "" H 1650 5450 50  0001 C CNN
	5    1650 5450
	1    0    0    -1  
$EndComp
$Comp
L gbtx:igloo U?
U 6 1 5DDB16A6
P 4900 5400
F 0 "U?" H 4500 5350 50  0000 L CNN
F 1 "igloo" H 5150 5400 50  0000 L CNN
F 2 "" H 4900 5400 50  0001 C CNN
F 3 "" H 4900 5400 50  0001 C CNN
	6    4900 5400
	1    0    0    -1  
$EndComp
$EndSCHEMATC
