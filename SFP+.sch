EESchema Schematic File Version 4
LIBS:gbtx-testboard-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L gbtx:74441-0001 U?
U 1 1 5D7A2D73
P 2100 2700
F 0 "U?" H 1850 2900 50  0000 C CNN
F 1 "74441-0001" H 2050 1700 50  0000 C CNN
F 2 "" H 2100 2800 50  0001 C CNN
F 3 "" H 2100 2800 50  0001 C CNN
	1    2100 2700
	1    0    0    -1  
$EndComp
$Comp
L gbtx:747540420 U?
U 1 1 5D7A5A71
P 2250 5100
F 0 "U?" H 1900 5650 50  0000 C CNN
F 1 "747540420" H 2050 3700 50  0000 C CNN
F 2 "" H 2250 5100 50  0001 C CNN
F 3 "" H 2250 5100 50  0001 C CNN
	1    2250 5100
	1    0    0    -1  
$EndComp
Text HLabel 1300 6550 0    50   Input ~ 0
GND
Wire Wire Line
	1750 4700 1500 4700
Wire Wire Line
	1500 4700 1500 4800
Wire Wire Line
	1750 6300 1500 6300
Wire Wire Line
	1750 6200 1500 6200
Connection ~ 1500 6200
Wire Wire Line
	1500 6200 1500 6300
Wire Wire Line
	1750 6100 1500 6100
Connection ~ 1500 6100
Wire Wire Line
	1500 6100 1500 6200
Wire Wire Line
	1750 6000 1500 6000
Connection ~ 1500 6000
Wire Wire Line
	1500 6000 1500 6100
Wire Wire Line
	1750 5900 1500 5900
Connection ~ 1500 5900
Wire Wire Line
	1500 5900 1500 6000
Wire Wire Line
	1750 5800 1500 5800
Connection ~ 1500 5800
Wire Wire Line
	1500 5800 1500 5900
Wire Wire Line
	1750 5700 1500 5700
Connection ~ 1500 5700
Wire Wire Line
	1500 5700 1500 5800
Wire Wire Line
	1750 5600 1500 5600
Connection ~ 1500 5600
Wire Wire Line
	1500 5600 1500 5700
Wire Wire Line
	1750 5500 1500 5500
Connection ~ 1500 5500
Wire Wire Line
	1500 5500 1500 5600
Wire Wire Line
	1750 5400 1500 5400
Connection ~ 1500 5400
Wire Wire Line
	1500 5400 1500 5500
Wire Wire Line
	1750 5300 1500 5300
Connection ~ 1500 5300
Wire Wire Line
	1500 5300 1500 5400
Wire Wire Line
	1750 5200 1500 5200
Connection ~ 1500 5200
Wire Wire Line
	1500 5200 1500 5300
Wire Wire Line
	1750 5100 1500 5100
Connection ~ 1500 5100
Wire Wire Line
	1500 5100 1500 5200
Wire Wire Line
	1750 5000 1500 5000
Connection ~ 1500 5000
Wire Wire Line
	1500 5000 1500 5100
Wire Wire Line
	1750 4900 1500 4900
Connection ~ 1500 4900
Wire Wire Line
	1500 4900 1500 5000
Wire Wire Line
	1750 4800 1500 4800
Connection ~ 1500 4800
Wire Wire Line
	1500 4800 1500 4900
Wire Wire Line
	2700 4700 2950 4700
Text HLabel 1500 3550 0    50   Input ~ 0
GND
Text HLabel 3000 3550 2    50   Input ~ 0
GND
Text HLabel 3150 6500 2    50   Input ~ 0
GND
Wire Wire Line
	3150 6500 2950 6500
Wire Wire Line
	1300 6550 1500 6550
Wire Wire Line
	1500 6550 1500 6300
Connection ~ 1500 6300
Wire Wire Line
	2950 4700 2950 4800
Wire Wire Line
	2700 4800 2950 4800
Connection ~ 2950 4800
Wire Wire Line
	2950 4800 2950 4900
Wire Wire Line
	2700 4900 2950 4900
Connection ~ 2950 4900
Wire Wire Line
	2950 4900 2950 5000
Wire Wire Line
	2700 5000 2950 5000
Connection ~ 2950 5000
Wire Wire Line
	2950 5000 2950 5100
Wire Wire Line
	2700 5100 2950 5100
Connection ~ 2950 5100
Wire Wire Line
	2950 5100 2950 5200
Wire Wire Line
	2700 5200 2950 5200
Connection ~ 2950 5200
Wire Wire Line
	2950 5200 2950 5300
Wire Wire Line
	2700 5300 2950 5300
Connection ~ 2950 5300
Wire Wire Line
	2950 5300 2950 5400
Wire Wire Line
	2700 5400 2950 5400
Connection ~ 2950 5400
Wire Wire Line
	2950 5400 2950 5500
Wire Wire Line
	2700 5500 2950 5500
Connection ~ 2950 5500
Wire Wire Line
	2950 5500 2950 5600
Wire Wire Line
	2700 5600 2950 5600
Connection ~ 2950 5600
Wire Wire Line
	2950 5600 2950 5700
Wire Wire Line
	2700 5700 2950 5700
Connection ~ 2950 5700
Wire Wire Line
	2950 5700 2950 5800
Wire Wire Line
	2700 5800 2950 5800
Connection ~ 2950 5800
Wire Wire Line
	2950 5800 2950 5900
Wire Wire Line
	2700 5900 2950 5900
Connection ~ 2950 5900
Wire Wire Line
	2950 5900 2950 6000
Wire Wire Line
	2700 6000 2950 6000
Connection ~ 2950 6000
Wire Wire Line
	2950 6000 2950 6100
Wire Wire Line
	2700 6100 2950 6100
Connection ~ 2950 6100
Wire Wire Line
	2950 6100 2950 6200
Wire Wire Line
	2700 6200 2950 6200
Connection ~ 2950 6200
Wire Wire Line
	2950 6200 2950 6300
Wire Wire Line
	2700 6300 2950 6300
Connection ~ 2950 6300
Wire Wire Line
	2950 6300 2950 6500
Wire Wire Line
	1700 3550 1650 3550
Wire Wire Line
	1700 2650 1650 2650
Wire Wire Line
	1650 2650 1650 3550
Connection ~ 1650 3550
Wire Wire Line
	1650 3550 1500 3550
Wire Wire Line
	2800 2650 2850 2650
Wire Wire Line
	2850 2650 2850 2950
Wire Wire Line
	2850 3550 3000 3550
Wire Wire Line
	2800 3550 2850 3550
Connection ~ 2850 3550
Text HLabel 5100 1250 0    50   Input ~ 0
3V3
$Comp
L Device:C_Small C?
U 1 1 5DE16916
P 5450 1500
F 0 "C?" H 5542 1546 50  0000 L CNN
F 1 "100n" H 5542 1455 50  0000 L CNN
F 2 "" H 5450 1500 50  0001 C CNN
F 3 "~" H 5450 1500 50  0001 C CNN
	1    5450 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5DE1DAD7
P 6450 1500
F 0 "C?" H 6542 1546 50  0000 L CNN
F 1 "100n" H 6542 1455 50  0000 L CNN
F 2 "" H 6450 1500 50  0001 C CNN
F 3 "~" H 6450 1500 50  0001 C CNN
	1    6450 1500
	1    0    0    -1  
$EndComp
Text Label 3350 3050 2    50   ~ 0
SFP+_VCCT
Wire Wire Line
	3350 3050 2800 3050
Text Label 3350 3150 2    50   ~ 0
SFP+_VCCR
Wire Wire Line
	3350 3150 2800 3150
Wire Wire Line
	2800 3250 2850 3250
Connection ~ 2850 3250
Wire Wire Line
	2850 3250 2850 3550
Wire Wire Line
	2800 2950 2850 2950
Connection ~ 2850 2950
Wire Wire Line
	2850 2950 2850 3250
$Comp
L pspice:INDUCTOR L?
U 1 1 5DE31044
P 6000 1250
F 0 "L?" H 6000 1465 50  0000 C CNN
F 1 "4.7u" H 6000 1374 50  0000 C CNN
F 2 "" H 6000 1250 50  0001 C CNN
F 3 "~" H 6000 1250 50  0001 C CNN
	1    6000 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5DE33623
P 6850 1500
F 0 "C?" H 6942 1546 50  0000 L CNN
F 1 "22u" H 6942 1455 50  0000 L CNN
F 2 "" H 6850 1500 50  0001 C CNN
F 3 "~" H 6850 1500 50  0001 C CNN
	1    6850 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 1250 5450 1250
Wire Wire Line
	5450 1400 5450 1250
Connection ~ 5450 1250
Wire Wire Line
	5450 1250 5750 1250
Wire Wire Line
	6250 1250 6450 1250
Wire Wire Line
	6850 1250 6850 1400
Wire Wire Line
	6450 1250 6450 1400
Connection ~ 6450 1250
Wire Wire Line
	6450 1250 6850 1250
Text HLabel 5100 1700 0    50   Input ~ 0
GND
Text Label 7300 1250 2    50   ~ 0
SFP+_VCCT
Wire Wire Line
	7300 1250 6850 1250
Connection ~ 6850 1250
Text HLabel 7700 1250 0    50   Input ~ 0
3V3
$Comp
L Device:C_Small C?
U 1 1 5DE48D4B
P 8050 1500
F 0 "C?" H 8142 1546 50  0000 L CNN
F 1 "100n" H 8142 1455 50  0000 L CNN
F 2 "" H 8050 1500 50  0001 C CNN
F 3 "~" H 8050 1500 50  0001 C CNN
	1    8050 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5DE48D55
P 9050 1500
F 0 "C?" H 9142 1546 50  0000 L CNN
F 1 "100n" H 9142 1455 50  0000 L CNN
F 2 "" H 9050 1500 50  0001 C CNN
F 3 "~" H 9050 1500 50  0001 C CNN
	1    9050 1500
	1    0    0    -1  
$EndComp
$Comp
L pspice:INDUCTOR L?
U 1 1 5DE48D5F
P 8600 1250
F 0 "L?" H 8600 1465 50  0000 C CNN
F 1 "4.7u" H 8600 1374 50  0000 C CNN
F 2 "" H 8600 1250 50  0001 C CNN
F 3 "~" H 8600 1250 50  0001 C CNN
	1    8600 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5DE48D69
P 9450 1500
F 0 "C?" H 9542 1546 50  0000 L CNN
F 1 "22u" H 9542 1455 50  0000 L CNN
F 2 "" H 9450 1500 50  0001 C CNN
F 3 "~" H 9450 1500 50  0001 C CNN
	1    9450 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 1250 8050 1250
Wire Wire Line
	8050 1400 8050 1250
Connection ~ 8050 1250
Wire Wire Line
	8050 1250 8350 1250
Wire Wire Line
	8850 1250 9050 1250
Wire Wire Line
	9450 1250 9450 1400
Wire Wire Line
	9050 1250 9050 1400
Connection ~ 9050 1250
Wire Wire Line
	9050 1250 9450 1250
Text HLabel 7700 1700 0    50   Input ~ 0
GND
Text Label 9900 1250 2    50   ~ 0
SFP+_VCCR
Wire Wire Line
	9900 1250 9450 1250
Connection ~ 9450 1250
Wire Wire Line
	5100 1700 5450 1700
Wire Wire Line
	7700 1700 8050 1700
Wire Wire Line
	9450 1600 9450 1700
Wire Wire Line
	9050 1600 9050 1700
Connection ~ 9050 1700
Wire Wire Line
	9050 1700 9450 1700
Wire Wire Line
	8050 1600 8050 1700
Connection ~ 8050 1700
Wire Wire Line
	8050 1700 9050 1700
Wire Wire Line
	6850 1600 6850 1700
Wire Wire Line
	6450 1550 6450 1600
Connection ~ 6450 1700
Wire Wire Line
	6450 1700 6850 1700
Connection ~ 6450 1600
Wire Wire Line
	6450 1600 6450 1700
Wire Wire Line
	5450 1600 5450 1700
Connection ~ 5450 1700
Wire Wire Line
	5450 1700 6450 1700
Text Notes 1500 4450 0    50   ~ 0
Shield
Text Notes 1500 2350 0    50   ~ 0
SFP+ Connector
Wire Wire Line
	3350 3350 2800 3350
Wire Wire Line
	3350 3450 2800 3450
Text HLabel 3350 3350 2    50   Input ~ 0
SFP+_RD_P
Text HLabel 3350 3450 2    50   Input ~ 0
SFP+_RD_N
Text HLabel 3350 2750 2    50   Input ~ 0
SFP+_TD_N
Text HLabel 3350 2850 2    50   Input ~ 0
SFP+_TD_P
Wire Wire Line
	3350 2750 2800 2750
Wire Wire Line
	3350 2850 2800 2850
Text Notes 850  750  0    129  ~ 0
SFP+
$EndSCHEMATC
